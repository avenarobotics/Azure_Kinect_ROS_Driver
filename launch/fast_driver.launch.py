"""Launch a talker and a listener in a component container."""
import os
import sys
import launch
import yaml
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription, conditions
import launch.actions 
import launch_ros.actions 
from launch.event_handlers import OnProcessExit
# from launch.actions import LogInfo
# from launch.actions import (DeclareLaunchArgument, GroupAction, IncludeLaunchDescription)
# from launch_ros.actions import ComposableNodeContainer
from launch.substitutions import LaunchConfiguration, Command

from launch.launch_description_sources import PythonLaunchDescriptionSource

import launch.actions
import launch_ros.actions

extra_arguments = {'use_intra_process_comms': True}


cam_params = [
    {'cam1_depth_enabled': True},
    {'cam1_depth_mode': "WFOV_2X2BINNED"},
    {'cam1_color_enabled': True},
    {'cam1_color_format': "bgra"},
    {'cam1_color_resolution': "720P"},
    {'cam1_fps': 5},
    {'cam1_point_cloud': True},
    {'cam1_rgb_point_cloud': False},
    {'cam1_point_cloud_in_depth_frame': True},
    {'cam1_sensor_sn': ""},
    {'cam1_recording_file': ""},
    {'cam1_recording_loop_enabled': False},
    {'cam1_body_tracking_enabled': False},
    {'cam1_body_tracking_smoothing_factor': 1.0},
    {'cam1_rescale_ir_to_mono8': False},
    {'cam1_ir_mono8_scaling_factor': 1.0},
    {'cam1_imu_rate_target': 0},
    {'cam1_wired_sync_mode': 1},
    {'cam1_subordinate_delay_off_master_usec': 0},

    {'cam2_depth_enabled': True},
    {'cam2_depth_mode': "WFOV_2X2BINNED"},
    {'cam2_color_enabled': True},
    {'cam2_color_format': "bgra"},
    {'cam2_color_resolution': "720P"},
    {'cam2_fps': 5},
    {'cam2_point_cloud': True},
    {'cam2_rgb_point_cloud': False},
    {'cam2_point_cloud_in_depth_frame': True},
    {'cam2_sensor_sn': ""},
    {'cam2_recording_file': ""},
    {'cam2_recording_loop_enabled': False},
    {'cam2_body_tracking_enabled': False},
    {'cam2_body_tracking_smoothing_factor': 1.0},
    {'cam2_rescale_ir_to_mono8': False},
    {'cam2_ir_mono8_scaling_factor': 1.0},
    {'cam2_imu_rate_target': 0},
    {'cam2_wired_sync_mode': 2},
    {'cam2_subordinate_delay_off_master_usec': 2000},

    {'depth_change_factor': 0.01},
    {'smoothing_size': 4.0},
    {'shadow_treshold': 0.5},
    {'rect_size': 5}
]


def generate_launch_description():
    """Generate launch description with multiple components."""

    driver_container = launch_ros.actions.ComposableNodeContainer(
        name='driver_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
            ComposableNode(
                package='azure_kinect_ros_driver',
                plugin='K4AFastPublisher',
                name='azure_kinect_ros_driver',
                extra_arguments=[extra_arguments],
                parameters=cam_params,
            )
        ],
        output='both',
    )

    ld = launch.LaunchDescription([driver_container])

    return ld

