// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

#ifndef K4A_CALIBRATION_TRANSFORM_DATA_H
#define K4A_CALIBRATION_TRANSFORM_DATA_H

#define _USE_MATH_DEFINES

// System headers
//
#include <vector>

// Library headers
//
#include <k4a/k4a.h>
#include <k4a/k4a.hpp>
#include <k4arecord/playback.hpp>
#include "rclcpp/rclcpp.hpp"
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <sensor_msgs/msg/camera_info.hpp>

// Project headers
//
#include "azure_kinect_ros_driver/cameras_data/k4a_ros_device_params.h"

class K4ACalibrationTransformData
{
public:
  K4ACalibrationTransformData(const std::string& prefix);
  void initialize(const k4a::device& device, const k4a_depth_mode_t depthMode, const k4a_color_resolution_t resolution,
                  const K4AROSDeviceParams& params);
  void initialize(const k4a::playback& k4a_playback_handle, const K4AROSDeviceParams& params);
  int getDepthWidth();
  int getDepthHeight();
  int getColorWidth();
  int getColorHeight();
  void getDepthCameraInfo(sensor_msgs::msg::CameraInfo& camera_info);
  void getRgbCameraInfo(sensor_msgs::msg::CameraInfo& camera_info);

  k4a::calibration k4a_calibration_;
  k4a::transformation k4a_transformation_;

  k4a::image point_cloud_image_;
  k4a::image transformed_rgb_image_;
  k4a::image transformed_depth_image_;

  std::string tf_prefix_;
  std::string camera_base_frame_;
  std::string rgb_camera_frame_;
  std::string depth_camera_frame_;
  std::string imu_frame_;

private:
  void initialize(const K4AROSDeviceParams& params);

  tf2::Quaternion getDepthToBaseRotationCorrection();
  tf2::Vector3 getDepthToBaseTranslationCorrection();

  std::string _prefix;
};

#endif
