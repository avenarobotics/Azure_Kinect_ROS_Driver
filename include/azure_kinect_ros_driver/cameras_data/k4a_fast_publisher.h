#pragma once 

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/camera_info.hpp"
#include <k4a/k4a.hpp>
#include "k4a_ros_types.h"

#include "azure_kinect_ros_driver/cameras_data/k4a_ros_device_params.h"
#include "azure_kinect_ros_driver/cameras_data/k4a_calibration_transform_data.h"
#include <image_transport/image_transport.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include <custom_interfaces/msg/cameras_data.hpp>
#include <chrono>
#include "sensor_msgs/point_cloud2_iterator.hpp"
#include <thread>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl/filters/shadowpoints.h>
#include <pcl/features/normal_3d_omp.h>

// #include <pcl/gpu/containers/device_memory.hpp>

#include <std_msgs/msg/string.hpp>

#include <helpers_vision/visualization.hpp>
#include <helpers_vision/helpers_vision.hpp>


#include <pcl/io/pcd_io.h> 
#include <pcl/features/integral_image_normal.h>
// #include <pcl/gpu/features/features.hpp>
// #include <pcl/gpu/containers/initialization.h>


// #define DEBUG

struct DeviceAndParams
{
    std::string prefix;
    std::shared_ptr<K4AROSDeviceParams> ros_params;
    std::shared_ptr<K4ACalibrationTransformData> calibration_data;
    k4a::capture capture;
    std::shared_ptr<k4a::device> device;
    Eigen::Affine3f cam_transform;
};

class K4AFastPublisher : public rclcpp::Node
{
public:
    explicit K4AFastPublisher(const rclcpp::NodeOptions& options);
    ~K4AFastPublisher();

    //get data run publisher
    k4a_result_t startDevices();

    //start devices
    k4a_result_t initDevices(const rclcpp::NodeOptions& options);

    k4a_result_t stopDevices();
    k4a_result_t getPointCloud(const DeviceAndParams& device, std::shared_ptr<sensor_msgs::msg::PointCloud2>& point_cloud, std::shared_ptr<sensor_msgs::msg::Image>& depth_frame);
    k4a_result_t getRgbFrame(const DeviceAndParams& device, std::shared_ptr<sensor_msgs::msg::Image>& rgb_frame, bool rectified = false);
private:
    void publishColoredCloud(std::shared_ptr<sensor_msgs::msg::PointCloud2> point_cloud, std::shared_ptr<sensor_msgs::msg::Image> image);

    k4a_result_t renderBGR32ToROS(std::shared_ptr<sensor_msgs::msg::Image>& rgb_frame, k4a::image& k4a_bgra_frame);
    k4a_result_t renderDepthToROS(std::shared_ptr<sensor_msgs::msg::Image>& depth_image, k4a::image& k4a_depth_frame);
    k4a_result_t fillPointCloud(const k4a::image& pointcloud_image, pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud);
    k4a_result_t transformPointCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& out_point_cloud, const DeviceAndParams& device);
    k4a_result_t removeShadows(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& out_point_cloud, const DeviceAndParams& device);
    std::chrono::microseconds getCaptureTimestamp(const k4a::capture& capture);
    void setParams(std::shared_ptr<K4AROSDeviceParams>& ros_params, int camera_id);
    void getParams();

    rclcpp::Time timestampToROS(const std::chrono::microseconds& k4a_timestamp_us);
    rclcpp::Time timestampToROS(const uint64_t& k4a_timestamp_us);
    void initializeTimestampOffset(const std::chrono::microseconds& k4a_device_timestamp_us);
    void updateTimestampOffset(const std::chrono::microseconds& k4a_device_timestamp_us, const std::chrono::nanoseconds& k4a_system_timestamp_ns);
    k4a_result_t renderBGRA32ToROS(const DeviceAndParams& device, std::shared_ptr<sensor_msgs::msg::Image>& rgb_image,
                                                 k4a::image& k4a_bgra_frame);

    bool thread_1_done, thread_2_done;
    custom_interfaces::msg::CamerasData final_msg;
    void checkAndPublish();
    k4a_result_t runCamera(DeviceAndParams & device, int camera_id);

    std::vector<DeviceAndParams> _devices;
    rclcpp::Publisher<custom_interfaces::msg::CamerasData>::SharedPtr _cameras_data_publisher;
    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr _camera_1_info_publisher;
    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr _camera_2_info_publisher;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _pointcloud_rgb_1_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _pointcloud_rgb_2_debug_publisher;

    pcl::visualization::PCLVisualizer::Ptr viewer;

    float _depth_change_factor;
    float _smoothing_size;
    float _shadow_treshold;
    int _rect_size;

    rclcpp::TimerBase::SharedPtr _timer;
    bool stop = false;

    helpers::SubscriptionsManager::SharedPtr _subscriptions_manager;

    std::thread t1;
    std::thread t2;
    
#ifdef DEBUG
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _pointcloud_1_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _pointcloud_2_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _rgb_1_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _rgb_2_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _depth_1_debug_publisher;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _depth_2_debug_publisher;
#endif

    std::chrono::nanoseconds _device_to_realtime_offset{0};
};