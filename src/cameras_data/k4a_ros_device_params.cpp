// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// Associated header
//
#include "azure_kinect_ros_driver/cameras_data/k4a_ros_device_params.h"

// System headers
//

// Library headers
//
#include <k4a/k4a.h>

// Project headers
//

K4AROSDeviceParams::K4AROSDeviceParams(const std::string& prefix)
{
  _prefix = prefix;
}

k4a_result_t K4AROSDeviceParams::GetDeviceConfig(k4a_device_configuration_t* configuration)
{
  configuration->depth_delay_off_color_usec = 0;
  configuration->disable_streaming_indicator = false;

  if (wired_sync_mode == 0)
  {
    configuration->wired_sync_mode = K4A_WIRED_SYNC_MODE_STANDALONE;
  }
  else if (wired_sync_mode == 1)
  {
    configuration->wired_sync_mode = K4A_WIRED_SYNC_MODE_MASTER;
  }
  else if (wired_sync_mode == 2)
  {
    configuration->wired_sync_mode = K4A_WIRED_SYNC_MODE_SUBORDINATE;
  }
  else
  {
    std::cout << "Wrong wired sync mode: " << wired_sync_mode << std::endl;
    return K4A_RESULT_FAILED;
  }


  configuration->subordinate_delay_off_master_usec = subordinate_delay_off_master_usec;

  if (!color_enabled)
  {
  

    configuration->color_resolution = K4A_COLOR_RESOLUTION_OFF;
  }
  else
  {
    if (color_format == "jpeg")
    {
      configuration->color_format = K4A_IMAGE_FORMAT_COLOR_MJPG;
    }
    else if (color_format == "bgra")
    {
      configuration->color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
    }
    else
    {
      std::cout << "Wrong color format: " << configuration->color_format << std::endl;
      return K4A_RESULT_FAILED;
    }

    if (color_resolution == "720P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_720P;
    }
    else if (color_resolution == "1080P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_1080P;
    }
    else if (color_resolution == "1440P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_1440P;
    }
    else if (color_resolution == "1536P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_1536P;
    }
    else if (color_resolution == "2160P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_2160P;
    }
    else if (color_resolution == "3072P")
    {
      configuration->color_resolution = K4A_COLOR_RESOLUTION_3072P;
    }
    else
    {
      std::cout << "Wrong color resolution: " << std::endl;
      return K4A_RESULT_FAILED;
    }
  }

  if (!depth_enabled)
  {
    configuration->depth_mode = K4A_DEPTH_MODE_OFF;
  }
  else
  {
    if (depth_mode == "NFOV_2X2BINNED")
    {
      configuration->depth_mode = K4A_DEPTH_MODE_NFOV_2X2BINNED;
    }
    else if (depth_mode == "NFOV_UNBINNED")
    {
      configuration->depth_mode = K4A_DEPTH_MODE_NFOV_UNBINNED;
    }
    else if (depth_mode == "WFOV_2X2BINNED")
    {
      configuration->depth_mode = K4A_DEPTH_MODE_WFOV_2X2BINNED;
    }
    else if (depth_mode == "WFOV_UNBINNED")
    {
      configuration->depth_mode = K4A_DEPTH_MODE_WFOV_UNBINNED;
    }
    else if (depth_mode == "PASSIVE_IR")
    {
      configuration->depth_mode = K4A_DEPTH_MODE_PASSIVE_IR;
    }
    else
    {
      std::cout << "Wrong depth mode : " << configuration->depth_mode << std::endl;
      return K4A_RESULT_FAILED;
    }
  }

  if (fps == 5)
  {
    configuration->camera_fps = K4A_FRAMES_PER_SECOND_5;
  }
  else if (fps == 15)
  {
    configuration->camera_fps = K4A_FRAMES_PER_SECOND_15;
  }
  else if (fps == 30)
  {
    configuration->camera_fps = K4A_FRAMES_PER_SECOND_30;
  }
  else
  {
    std::cout << "Wrong fps: " << fps << std::endl;
    return K4A_RESULT_FAILED;
  }

  // Ensure that if RGB and depth cameras are enabled, we ask for synchronized frames
  if (depth_enabled && color_enabled)
  {
    configuration->synchronized_images_only = true;
  }
  else
  {
    configuration->synchronized_images_only = false;
  }

  // Ensure that the "point_cloud" option is not used with passive IR mode, since they are incompatible
  if (point_cloud && (configuration->depth_mode == K4A_DEPTH_MODE_PASSIVE_IR))
  {
      return K4A_RESULT_FAILED;
  }

  // Ensure that point_cloud is enabled if using rgb_point_cloud
  if (rgb_point_cloud && !point_cloud)
  {
      return K4A_RESULT_FAILED;
  }

  // Ensure that color camera is enabled when generating a color point cloud
  if (rgb_point_cloud && !color_enabled)
  {
      return K4A_RESULT_FAILED;
  }

  // Ensure that color image contains RGB pixels instead of compressed JPEG data.
  if (rgb_point_cloud && color_format == "jpeg")
  {
      return K4A_RESULT_FAILED;
  }

  // Ensure that target IMU rate is feasible
  if (imu_rate_target == 0)
  {
    imu_rate_target = IMU_MAX_RATE;
  
  }

  if (imu_rate_target < 0 || imu_rate_target > IMU_MAX_RATE)
  {
      return K4A_RESULT_FAILED;
  }

  int div = IMU_MAX_RATE / imu_rate_target;
  float imu_rate_rounded = IMU_MAX_RATE / div;
  // Since we will throttle the IMU by averaging div samples together, this is the
  // achievable rate when rouded to the nearest whole number div.

  return K4A_RESULT_SUCCEEDED;
}

void K4AROSDeviceParams::Help()
{
#define LIST_ENTRY(param_variable, param_help_string, param_type, param_default_val)                                   \


  ROS_PARAM_LIST
#undef LIST_ENTRY
}

void K4AROSDeviceParams::Print()
{
#define LIST_ENTRY(param_variable, param_help_string, param_type, param_default_val)                                   \


  ROS_PARAM_LIST
#undef LIST_ENTRY
}
