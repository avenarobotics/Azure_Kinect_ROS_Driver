// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// Associated header
//
#include "azure_kinect_ros_driver/cameras_data/k4a_calibration_transform_data.h"

// System headers
//
#include <stdexcept>

// Library headers
//
#include <angles/angles.h>
#include <sensor_msgs/distortion_models.hpp>
#include <tf2/LinearMath/Transform.h>
#include <tf2/convert.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

// Project headers
//
#include "azure_kinect_ros_driver/cameras_data/k4a_ros_types.h"
K4ACalibrationTransformData::K4ACalibrationTransformData(const std::string& prefix)
{
  _prefix = prefix;
  tf_prefix_ = prefix;
  camera_base_frame_ = _prefix+"/camera_base";
  rgb_camera_frame_ = _prefix+"/rgb_camera_link";
  depth_camera_frame_ = _prefix+"/depth_camera_link";
  imu_frame_ = _prefix+"/imu_link";
}
void K4ACalibrationTransformData::initialize(const k4a::device& device, const k4a_depth_mode_t depth_mode,
                                             const k4a_color_resolution_t resolution, const K4AROSDeviceParams& params)
{
  k4a_calibration_ = device.get_calibration(depth_mode, resolution);
  initialize(params);
}

void K4ACalibrationTransformData::initialize(const k4a::playback& k4a_playback_handle, const K4AROSDeviceParams& params)
{
  k4a_calibration_ = k4a_playback_handle.get_calibration();
  initialize(params);
}

void K4ACalibrationTransformData::initialize(const K4AROSDeviceParams& params)
{
  k4a_transformation_ = k4a::transformation(k4a_calibration_);
  tf_prefix_ = params.tf_prefix;

  bool depthEnabled = (getDepthWidth() * getDepthHeight() > 0);
  bool colorEnabled = (getColorWidth() * getColorHeight() > 0);

  // Create a buffer to store the point cloud
  if (params.point_cloud && (!params.rgb_point_cloud || params.point_cloud_in_depth_frame))
  {
    point_cloud_image_ = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, getDepthWidth(), getDepthHeight(),
                                            getDepthWidth() * 3 * (int) sizeof(DepthPixel));
  }
  else if (params.point_cloud && params.rgb_point_cloud)
  {
    point_cloud_image_ = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, getColorWidth(), getColorHeight(),
                                            getColorWidth() * 3 * (int) sizeof(DepthPixel));
  }

  if (depthEnabled && colorEnabled)
  {
    // Create a buffer to store RGB images that are transformed into the depth camera geometry
    transformed_rgb_image_ = k4a::image::create(K4A_IMAGE_FORMAT_COLOR_BGRA32, getDepthWidth(), getDepthHeight(),
                                                getDepthWidth() * (int) sizeof(BgraPixel));

    // Create a buffer to store depth images that are transformed into the RGB camera geometry
    transformed_depth_image_ = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, getColorWidth(), getColorHeight(),
                                                  getColorWidth() * (int) sizeof(DepthPixel));
  }
}

int K4ACalibrationTransformData::getDepthWidth() { return k4a_calibration_.depth_camera_calibration.resolution_width; }

int K4ACalibrationTransformData::getDepthHeight()
{
  return k4a_calibration_.depth_camera_calibration.resolution_height;
}

int K4ACalibrationTransformData::getColorWidth() { return k4a_calibration_.color_camera_calibration.resolution_width; }

int K4ACalibrationTransformData::getColorHeight()
{
  return k4a_calibration_.color_camera_calibration.resolution_height;
}

// The [0,0,0] center of the URDF, the TF frame known as "camera_base", is offset slightly from the
// [0,0,0] origin of the depth camera frame, known as "depth_camera_link" or "depth_camera_frame"
//
// Publish a TF link so the URDF model and the depth camera line up correctly
#define DEPTH_CAMERA_OFFSET_MM_X 0.0f
#define DEPTH_CAMERA_OFFSET_MM_Y 0.0f
#define DEPTH_CAMERA_OFFSET_MM_Z 1.8f  // The depth camera is shifted 1.8mm up in the depth window

tf2::Vector3 K4ACalibrationTransformData::getDepthToBaseTranslationCorrection()
{
  // These are purely cosmetic tranformations for the URDF drawing!!
  return tf2::Vector3(DEPTH_CAMERA_OFFSET_MM_X / 1000.0f, DEPTH_CAMERA_OFFSET_MM_Y / 1000.0f,
                      DEPTH_CAMERA_OFFSET_MM_Z / 1000.0f);
}

tf2::Quaternion K4ACalibrationTransformData::getDepthToBaseRotationCorrection()
{
  // These are purely cosmetic tranformations for the URDF drawing!!
  tf2::Quaternion ros_camera_rotation;  // ROS camera co-ordinate system requires rotating the entire camera relative to
                                        // camera_base
  tf2::Quaternion depth_rotation;       // K4A has one physical camera that is about 6 degrees downward facing.

  depth_rotation.setEuler(0, angles::from_degrees(-6.0), 0);
  ros_camera_rotation.setEuler(M_PI / -2.0f, M_PI, (M_PI / 2.0f));

  return ros_camera_rotation * depth_rotation;
}

void K4ACalibrationTransformData::getDepthCameraInfo(sensor_msgs::msg::CameraInfo& camera_info)
{
  camera_info.header.frame_id = tf_prefix_ + depth_camera_frame_;
  camera_info.width = getDepthWidth();
  camera_info.height = getDepthHeight();
  camera_info.distortion_model = sensor_msgs::distortion_models::RATIONAL_POLYNOMIAL;

  k4a_calibration_intrinsic_parameters_t* parameters = &k4a_calibration_.depth_camera_calibration.intrinsics.parameters;

  // The distortion parameters, size depending on the distortion model.
  // For "rational_polynomial", the 8 parameters are: (k1, k2, p1, p2, k3, k4, k5, k6).
  camera_info.d = {parameters->param.k1, parameters->param.k2, parameters->param.p1, parameters->param.p2,
                   parameters->param.k3, parameters->param.k4, parameters->param.k5, parameters->param.k6};
  
  // clang-format off
  // Intrinsic camera matrix for the raw (distorted) images.
  //     [fx  0 cx]
  // K = [ 0 fy cy]
  //     [ 0  0  1]
  // Projects 3D points in the camera coordinate frame to 2D pixel
  // coordinates using the focal lengths (fx, fy) and principal point
  // (cx, cy).
  camera_info.k = {parameters->param.fx,  0.0f,                   parameters->param.cx,
                   0.0f,                  parameters->param.fy,   parameters->param.cy,
                   0.0f,                  0.0,                    1.0f};

  // Projection/camera matrix
  //     [fx'  0  cx' Tx]
  // P = [ 0  fy' cy' Ty]
  //     [ 0   0   1   0]
  // By convention, this matrix specifies the intrinsic (camera) matrix
  //  of the processed (rectified) image. That is, the left 3x3 portion
  //  is the normal camera intrinsic matrix for the rectified image.
  // It projects 3D points in the camera coordinate frame to 2D pixel
  //  coordinates using the focal lengths (fx', fy') and principal point
  //  (cx', cy') - these may differ from the values in K.
  // For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
  //  also have R = the identity and P[1:3,1:3] = K.
  camera_info.p = {parameters->param.fx,  0.0f,                   parameters->param.cx,   0.0f,
                   0.0f,                  parameters->param.fy,   parameters->param.cy,   0.0f,
                   0.0f,                  0.0,                    1.0f,                   0.0f};

  // Rectification matrix (stereo cameras only)
  // A rotation matrix aligning the camera coordinate system to the ideal
  // stereo image plane so that epipolar lines in both stereo images are
  // parallel.
  camera_info.r = {1.0f, 0.0f, 0.0f,
                   0.0f, 1.0f, 0.0f,
                   0.0f, 0.0f, 1.0f};
  // clang-format on
}

void K4ACalibrationTransformData::getRgbCameraInfo(sensor_msgs::msg::CameraInfo& camera_info)
{
  camera_info.header.frame_id = rgb_camera_frame_;
  camera_info.width = getColorWidth();
  camera_info.height = getColorHeight();
  camera_info.distortion_model = sensor_msgs::distortion_models::RATIONAL_POLYNOMIAL;

  k4a_calibration_intrinsic_parameters_t* parameters = &k4a_calibration_.color_camera_calibration.intrinsics.parameters;
  // std::cout << "dpeth: " << k4a_calibration_.depth_camera_calibration.intrinsics.parameters.param.cx << std::endl;
  // std::cout << "rgb: " << parameters->param.cx << std::endl;
  // The distortion parameters, size depending on the distortion model.
  // For "rational_polynomial", the 8 parameters are: (k1, k2, p1, p2, k3, k4, k5, k6).
  camera_info.d = {parameters->param.k1, parameters->param.k2, parameters->param.p1, parameters->param.p2,
                   parameters->param.k3, parameters->param.k4, parameters->param.k5, parameters->param.k6};

  // clang-format off
  // Intrinsic camera matrix for the raw (distorted) images.
  //     [fx  0 cx]
  // K = [ 0 fy cy]
  //     [ 0  0  1]
  // Projects 3D points in the camera coordinate frame to 2D pixel
  // coordinates using the focal lengths (fx, fy) and principal point
  // (cx, cy).
  camera_info.k = {parameters->param.fx,  0.0f,                   parameters->param.cx,
                   0.0f,                  parameters->param.fy,   parameters->param.cy,
                   0.0f,                  0.0,                    1.0f};

  // Projection/camera matrix
  //     [fx'  0  cx' Tx]
  // P = [ 0  fy' cy' Ty]
  //     [ 0   0   1   0]
  // By convention, this matrix specifies the intrinsic (camera) matrix
  //  of the processed (rectified) image. That is, the left 3x3 portion
  //  is the normal camera intrinsic matrix for the rectified image.
  // It projects 3D points in the camera coordinate frame to 2D pixel
  //  coordinates using the focal lengths (fx', fy') and principal point
  //  (cx', cy') - these may differ from the values in K.
  // For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
  //  also have R = the identity and P[1:3,1:3] = K.
  camera_info.p = {parameters->param.fx,  0.0f,                   parameters->param.cx,   0.0f,
                   0.0f,                  parameters->param.fy,   parameters->param.cy,   0.0f,
                   0.0f,                  0.0,                    1.0f,                   0.0f};

  // Rectification matrix (stereo cameras only)
  // A rotation matrix aligning the camera coordinate system to the ideal
  // stereo image plane so that epipolar lines in both stereo images are
  // parallel.
  camera_info.r = {1.0f, 0.0f, 0.0f,
                   0.0f, 1.0f, 0.0f,
                   0.0f, 0.0f, 1.0f};
  // clang-format on
}
