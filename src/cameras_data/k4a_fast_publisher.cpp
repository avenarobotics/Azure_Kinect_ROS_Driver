#include "azure_kinect_ros_driver/cameras_data/k4a_fast_publisher.h"

K4AFastPublisher::K4AFastPublisher(const rclcpp::NodeOptions& options) : Node("cameras_driver", options)
{
  helpers::commons::setLoggerLevel(get_logger(), "debug");
  initDevices(options);
  _timer = this->create_wall_timer(std::chrono::milliseconds(10), std::bind(&K4AFastPublisher::checkAndPublish, this));
  startDevices();

  std::cout << "--------------------------------INTRA----------------------------------" << std::endl;
  std::cout << options.use_intra_process_comms() << std::endl;
}

void K4AFastPublisher::setParams(std::shared_ptr<K4AROSDeviceParams>& ros_params, int camera_id)
{
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_depth_enabled");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_depth_mode");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_color_enabled");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_color_resolution");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_fps");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_color_format");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_point_cloud");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_rgb_point_cloud");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_point_cloud_in_depth_frame");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_sensor_sn");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_recording_file");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_recording_loop_enabled");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_body_tracking_enabled");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_body_tracking_smoothing_factor");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_rescale_ir_to_mono8");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_ir_mono8_scaling_factor");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_imu_rate_target");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_wired_sync_mode");
  this->declare_parameter("cam" + std::to_string(camera_id + 1) + "_subordinate_delay_off_master_usec");

  if (camera_id == 0)
  {
    this->declare_parameter("depth_change_factor");
    this->declare_parameter("smoothing_size");
    this->declare_parameter("shadow_treshold");
    this->declare_parameter("rect_size");
  }

  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_color_resolution", ros_params->color_resolution,
                   std::string("720P"));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_depth_mode", ros_params->depth_mode,
                   std::string("WFOV_UNBINNED"));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_depth_enabled", ros_params->depth_enabled, true);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_color_enabled", ros_params->color_enabled, true);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_color_resolution", ros_params->color_resolution,
                   std::string("720P"));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_fps", ros_params->fps, 5);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_color_format", ros_params->color_format,
                   std::string("bgra"));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_point_cloud", ros_params->point_cloud, true);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_rgb_point_cloud", ros_params->rgb_point_cloud, false);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_point_cloud_in_depth_frame",
                   ros_params->point_cloud_in_depth_frame, false);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_sensor_sn", ros_params->sensor_sn, std::string(""));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_recording_file", ros_params->recording_file,
                   std::string(""));
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_recording_loop_enabled",
                   ros_params->recording_loop_enabled, false);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_body_tracking_enabled", ros_params->body_tracking_enabled,
                   false);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_body_tracking_smoothing_factor",
                   ros_params->body_tracking_smoothing_factor, 0.0f);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_rescale_ir_to_mono8", ros_params->rescale_ir_to_mono8,
                   false);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_ir_mono8_scaling_factor",
                   ros_params->ir_mono8_scaling_factor, 1.0f);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_imu_rate_target", ros_params->imu_rate_target, 0);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_wired_sync_mode", ros_params->wired_sync_mode, 0);
  get_parameter_or("cam" + std::to_string(camera_id + 1) + "_subordinate_delay_off_master_usec",
                   ros_params->subordinate_delay_off_master_usec, 0);
  get_parameter_or("depth_change_factor", ros_params->depth_change_factor, 0.01f);
  get_parameter_or("smoothing_size", ros_params->smoothing_size, 0.2f);
  get_parameter_or("shadow_treshold", ros_params->shadow_treshold, 0.0f);
  get_parameter_or("rect_size", ros_params->rect_size, 2);

  _depth_change_factor = ros_params->depth_change_factor;
  _smoothing_size = ros_params->smoothing_size;
  _shadow_treshold = ros_params->shadow_treshold;
  _rect_size = ros_params->rect_size;
}

k4a_result_t K4AFastPublisher::initDevices(const rclcpp::NodeOptions& options)
{
  // helpers::Timer timer("Starting devices took", get_logger());
  uint32_t k4a_device_count = k4a::device::get_installed_count();
  RCLCPP_INFO(get_logger(), "There are %d devices connected to PC", k4a_device_count);

  for (uint32_t i = 0; i < k4a_device_count; i++)
  {
    DeviceAndParams sensor;
    int dupa = 0;
    sensor.prefix = "camera_" + std::to_string(i + 1);
    try
    {
      sensor.device = std::make_shared<k4a::device>(k4a::device::open(i));
      if (!*(sensor.device))
      {
        RCLCPP_ERROR(this->get_logger(), "Failed to open camera!");
        return K4A_RESULT_FAILED;
      }
      rclcpp::NodeOptions options2;
      options2.use_intra_process_comms(true);
      sensor.ros_params = std::make_shared<K4AROSDeviceParams>(sensor.prefix);
      setParams(sensor.ros_params, i);
      sensor.calibration_data = std::make_shared<K4ACalibrationTransformData>(sensor.prefix);
      k4a_device_configuration_t k4a_configuration = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
      k4a_result_t result = sensor.ros_params->GetDeviceConfig(&k4a_configuration);
      if (result != K4A_RESULT_SUCCEEDED)
      {
        RCLCPP_ERROR(this->get_logger(), "Failed to generate a device configuration. Not starting camera!");
        return result;
      }
      sensor.calibration_data->initialize(*(sensor.device), k4a_configuration.depth_mode,
                                          k4a_configuration.color_resolution, *(sensor.ros_params));
      RCLCPP_INFO_STREAM(this->get_logger(), "STARTING CAMERA");
      sensor.calibration_data->tf_prefix_ = sensor.prefix + "/";

      sensor.device->start_cameras(&k4a_configuration);
      _devices.push_back(sensor);
    }
    catch (const std::exception& e)
    {
      RCLCPP_ERROR(get_logger(), "Failed to open K4A device at index: %d", i);
      RCLCPP_ERROR(get_logger(), e.what());
      return K4A_RESULT_FAILED;
    }
    RCLCPP_INFO_STREAM(get_logger(), "Successfully opened device with no.: " << sensor.device->get_serialnum());
  }

  RCLCPP_INFO(get_logger(), "Creating publisher");
  rclcpp::QoS qos_setting = rclcpp::QoS(rclcpp::KeepLast(1));

  _cameras_data_publisher = create_publisher<custom_interfaces::msg::CamerasData>("/cameras_data", qos_setting);
  _camera_1_info_publisher = create_publisher<sensor_msgs::msg::CameraInfo>("/camera_1/depth_to_rgb/camera_info", 1);
  _camera_2_info_publisher = create_publisher<sensor_msgs::msg::CameraInfo>("/camera_2/depth_to_rgb/camera_info", 1);

  _pointcloud_rgb_1_debug_publisher = create_publisher<sensor_msgs::msg::PointCloud2>("/camera_1/points2", 1);
  _pointcloud_rgb_2_debug_publisher = create_publisher<sensor_msgs::msg::PointCloud2>("/camera_2/points2", 1);

#ifdef DEBUG
  _pointcloud_1_debug_publisher = create_publisher<sensor_msgs::msg::PointCloud2>("/pointcloud1", 1);
  _pointcloud_2_debug_publisher = create_publisher<sensor_msgs::msg::PointCloud2>("/pointcloud2", 1);
  _rgb_1_debug_publisher = create_publisher<sensor_msgs::msg::Image>("/rgb1", 1);
  _rgb_2_debug_publisher = create_publisher<sensor_msgs::msg::Image>("/rgb2", 1);
  _depth_1_debug_publisher = create_publisher<sensor_msgs::msg::Image>("/depth1", 1);
  _depth_2_debug_publisher = create_publisher<sensor_msgs::msg::Image>("/depth2", 1);
#endif

  return K4A_RESULT_SUCCEEDED;
}

k4a_result_t K4AFastPublisher::startDevices()
{
  t1 = std::thread(std::bind(&K4AFastPublisher::runCamera, this, _devices[0], 0));
  t2 = std::thread(std::bind(&K4AFastPublisher::runCamera, this, _devices[1], 1));
  return K4A_RESULT_SUCCEEDED;
}

k4a_result_t K4AFastPublisher::stopDevices()
{
  return K4A_RESULT_SUCCEEDED;
}

rclcpp::Time K4AFastPublisher::timestampToROS(const std::chrono::microseconds& k4a_timestamp_us)
{
  if (_device_to_realtime_offset.count() == 0)
  {
    initializeTimestampOffset(k4a_timestamp_us);
  }

  std::chrono::nanoseconds timestamp_in_realtime = k4a_timestamp_us + _device_to_realtime_offset;
  rclcpp::Time ros_time(timestamp_in_realtime.count(), RCL_ROS_TIME);
  return ros_time;
}

// Converts a k4a_imu_sample_t timestamp to a ros::Time object
rclcpp::Time K4AFastPublisher::timestampToROS(const uint64_t& k4a_timestamp_us)
{
  return timestampToROS(std::chrono::microseconds(k4a_timestamp_us));
}

void K4AFastPublisher::initializeTimestampOffset(const std::chrono::microseconds& k4a_device_timestamp_us)
{
  // We have no better guess than "now".
  std::chrono::nanoseconds realtime_clock = std::chrono::system_clock::now().time_since_epoch();

  _device_to_realtime_offset = realtime_clock - k4a_device_timestamp_us;

  RCLCPP_WARN_STREAM(this->get_logger(), "Initializing the device to realtime offset based on wall clock: "
                                             << _device_to_realtime_offset.count() << " ns");
}

void K4AFastPublisher::checkAndPublish()
{
  if (thread_1_done && thread_2_done)
  {
    // helpers::Timer timer("Publishing data took: ", get_logger());
    _cameras_data_publisher->publish(final_msg);
    std::cout << "----------------------- NEW FRAME -----------------------" << std::endl;
    thread_2_done = thread_1_done = false;
  }
}

k4a_result_t K4AFastPublisher::runCamera(DeviceAndParams& device, int camera_id)
{
  rclcpp::Rate loop_rate(device.ros_params->fps);
  while (rclcpp::ok())
  {
    // helpers::Timer timer("Single camera loop iteration took: ", get_logger());
    if (!*(device.device))
    {
      RCLCPP_ERROR(this->get_logger(), "Failed to get camera device!");
      return K4A_RESULT_FAILED;
    }
    if (device.device->get_capture(&(device.capture), std::chrono::milliseconds(K4A_WAIT_INFINITE)))
    {
      {
        // //helpers::Timer timer("Getting single camera data from device " + std::to_string(camera_id) + " took: ",
        //  get_logger());
        std::shared_ptr<sensor_msgs::msg::PointCloud2> point_cloud = std::make_shared<sensor_msgs::msg::PointCloud2>();
        std::shared_ptr<sensor_msgs::msg::Image> image = std::make_shared<sensor_msgs::msg::Image>();
        std::shared_ptr<sensor_msgs::msg::Image> depth_image = std::make_shared<sensor_msgs::msg::Image>();

        getPointCloud(device, point_cloud, depth_image);
        getRgbFrame(device, image);

        
        sensor_msgs::msg::CameraInfo camera_info;
        device.calibration_data->getRgbCameraInfo(camera_info);

        if (this->count_subscribers("camera_2/points2") > 0 || this->count_subscribers("camera_1/points2") > 0) 
          publishColoredCloud(point_cloud, image);
        else{
          
       

        final_msg.header.frame_id = "none";
        final_msg.header.stamp = now();

        if (camera_id == 0 && !thread_1_done)
        {
          final_msg.cam1_ptcld = *point_cloud;
          final_msg.cam1_rgb = *image;
          final_msg.cam1_depth = *depth_image;
          _camera_1_info_publisher->publish(camera_info);
          thread_1_done = true;

#ifdef DEBUG
          //   point_cloud->header.frame_id = "world";
          _pointcloud_1_debug_publisher->publish(*point_cloud);
          _rgb_1_debug_publisher->publish(*image);
          _depth_1_debug_publisher->publish(*depth_image);
#endif
        }
        else if (camera_id == 1 && !thread_2_done)
        {
#ifdef DEBUG
          //   point_cloud->header.frame_id = "world";
          _pointcloud_2_debug_publisher->publish(*point_cloud);
          _rgb_2_debug_publisher->publish(*image);
          _depth_2_debug_publisher->publish(*depth_image);
#endif

          _camera_2_info_publisher->publish(camera_info);
          final_msg.cam2_ptcld = *point_cloud;
          final_msg.cam2_rgb = *image;
          final_msg.cam2_depth = *depth_image;
          thread_2_done = true;
        }


        }



      }
    }
    else
    {
      RCLCPP_ERROR(this->get_logger(), "Failed to get camera capture!");
      return K4A_RESULT_FAILED;
    }
    // rclcpp::spin_some(get_node_base_interface());
    loop_rate.sleep();
  }
  return K4A_RESULT_SUCCEEDED;
}

K4AFastPublisher::~K4AFastPublisher()
{
  if (t1.joinable() && t2.joinable())
  {
    t1.join();
    t2.join();
  }

}

void K4AFastPublisher:: publishColoredCloud(std::shared_ptr<sensor_msgs::msg::PointCloud2> point_cloud,
                                           std::shared_ptr<sensor_msgs::msg::Image> image)
{
  cv::Mat rgb;

  helpers::converters::rosImageToCV(*image, rgb);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(*point_cloud, cloud);

  if(cloud->points.size() != rgb.cols* rgb.rows)
    return;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_rgb(new pcl::PointCloud<pcl::PointXYZRGB>);

  pcl::copyPointCloud(*cloud, *cloud_rgb);

  for (int i = 0; i < rgb.cols; i++)
    for (int j = 0; j < rgb.rows; j++)
    {
      cv::Vec3b rgb_value = rgb.at<cv::Vec3b>(j, i);
      int index = j * rgb.cols + i;
      cloud_rgb->points[index].b = rgb_value[0];
      cloud_rgb->points[index].g = rgb_value[1];
      cloud_rgb->points[index].r = rgb_value[2];
    }
  std::shared_ptr<sensor_msgs::msg::PointCloud2> point_cloud_to_publish(new sensor_msgs::msg::PointCloud2);
  helpers::converters::pclToRosPtcld<pcl::PointXYZRGB>(cloud_rgb, *point_cloud_to_publish);

  point_cloud_to_publish->header.stamp = now();
  std::cout << point_cloud_to_publish->header.frame_id << std::endl;
  if (point_cloud->header.frame_id == "camera_1/rgb_camera_link")
    _pointcloud_rgb_1_debug_publisher->publish(*point_cloud_to_publish);
  else
    _pointcloud_rgb_2_debug_publisher->publish(*point_cloud_to_publish);
}

void K4AFastPublisher::updateTimestampOffset(const std::chrono::microseconds& k4a_device_timestamp_us,
                                             const std::chrono::nanoseconds& k4a_system_timestamp_ns)
{
  std::chrono::nanoseconds realtime_clock = std::chrono::system_clock::now().time_since_epoch();
  std::chrono::nanoseconds monotonic_clock = std::chrono::steady_clock::now().time_since_epoch();

  std::chrono::nanoseconds monotonic_to_realtime = realtime_clock - monotonic_clock;

  // Next figure out the other part (combined).
  std::chrono::nanoseconds device_to_realtime =
      k4a_system_timestamp_ns - k4a_device_timestamp_us + monotonic_to_realtime;
  // If we're over a second off, just snap into place.
  if (_device_to_realtime_offset.count() == 0 ||
      std::abs((_device_to_realtime_offset - device_to_realtime).count()) > 1e7)
  {
    RCLCPP_WARN_STREAM(this->get_logger(), "Initializing or re-initializing the device to realtime offset: "
                                               << device_to_realtime.count() << " ns");
    _device_to_realtime_offset = device_to_realtime;
  }
  else
  {
    // Low-pass filter!
    constexpr double alpha = 0.10;
    _device_to_realtime_offset = _device_to_realtime_offset +
                                 std::chrono::nanoseconds(static_cast<int64_t>(
                                     std::floor(alpha * (device_to_realtime - _device_to_realtime_offset).count())));
  }
}

k4a_result_t K4AFastPublisher::getPointCloud(const DeviceAndParams& device,
                                             std::shared_ptr<sensor_msgs::msg::PointCloud2>& point_cloud,
                                             std::shared_ptr<sensor_msgs::msg::Image>& depth_frame)
{
  // helpers::Timer timer("Getting pointcloud took", get_logger());
  k4a::image k4a_depth_frame = device.capture.get_depth_image();
  if (!k4a_depth_frame)
  {
    RCLCPP_ERROR(this->get_logger(), "Cannot render point cloud: no depth frame");
    return K4A_RESULT_FAILED;
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_cloud(new pcl::PointCloud<pcl::PointXYZ>);

  device.calibration_data->k4a_transformation_.depth_image_to_color_camera(
      k4a_depth_frame, &(device.calibration_data->transformed_depth_image_));

  cv::Mat depth_buffer_mat(device.calibration_data->transformed_depth_image_.get_height_pixels(),
                           device.calibration_data->transformed_depth_image_.get_width_pixels(), CV_16UC1,
                           device.calibration_data->transformed_depth_image_.get_buffer());

  cv::Mat new_image(device.calibration_data->transformed_depth_image_.get_height_pixels(),
                    device.calibration_data->transformed_depth_image_.get_width_pixels(), CV_32FC1);
  depth_buffer_mat.convertTo(new_image, CV_32FC1, 1.0 / 1000.0f);

  depth_frame =
      cv_bridge::CvImage(std_msgs::msg::Header(), sensor_msgs::image_encodings::TYPE_32FC1, new_image).toImageMsg();
  depth_frame->header.frame_id = device.calibration_data->rgb_camera_frame_;
  depth_frame->header.stamp = timestampToROS(k4a_depth_frame.get_device_timestamp());

  device.calibration_data->point_cloud_image_ = k4a::image::create(
      K4A_IMAGE_FORMAT_DEPTH16, device.calibration_data->transformed_depth_image_.get_width_pixels(),
      device.calibration_data->transformed_depth_image_.get_height_pixels(),
      device.calibration_data->transformed_depth_image_.get_width_pixels() * 3 * (int)sizeof(DepthPixel));

  device.calibration_data->k4a_transformation_.depth_image_to_point_cloud(
      device.calibration_data->transformed_depth_image_, K4A_CALIBRATION_TYPE_COLOR,
      &(device.calibration_data->point_cloud_image_));

  point_cloud->header.frame_id = device.calibration_data->rgb_camera_frame_;
  point_cloud->header.stamp = timestampToROS(k4a_depth_frame.get_device_timestamp());

  fillPointCloud(device.calibration_data->point_cloud_image_, pcl_cloud);

  // if (_pointcloud_rgb_1_debug_publisher->get_subscription_count() == 0)
    if (this->count_subscribers("camera_2/points2") == 0 && this->count_subscribers("camera_1/points2") == 0) 
     removeShadows(pcl_cloud, pcl_cloud, device);

  helpers::vision::passThroughFilter(pcl_cloud, "z", 0.001, 1.5);
  pcl_cloud->header.frame_id = device.calibration_data->rgb_camera_frame_;
  pcl_cloud->width = pcl_cloud->points.size();
  pcl_cloud->height = 1;

  helpers::converters::pclToRosPtcld<pcl::PointXYZ>(pcl_cloud, *point_cloud);

  point_cloud->header.stamp = timestampToROS(k4a_depth_frame.get_device_timestamp());
  point_cloud->is_dense = false;
  point_cloud->is_bigendian = false;

  return K4A_RESULT_SUCCEEDED;
}

k4a_result_t K4AFastPublisher::removeShadows(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud,
                                             pcl::PointCloud<pcl::PointXYZ>::Ptr& out_point_cloud,
                                             const DeviceAndParams& device)
{
  auto normal_estimation = [this](const pcl::PointCloud<pcl::PointXYZ>::Ptr& input_cloud,
                                  pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals) {
    // helpers::Timer tiemr("normals: ", true);
    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal>::Ptr ne(
        new pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal>);
    // ne->setNormalEstimationMethod(ne->COVARIANCE_MATRIX); // fastest ne->AVERAGE_DEPTH_CHANGE ne->SIMPLE_3D_GRADIENT
    // a bit slower
    // pcl::IndicesConstPtr indices(new pcl::Indices);
    // pcl::IndicesPtr indices(new pcl::Indices);
    // indices->indices
    ne->setNormalEstimationMethod(ne->AVERAGE_DEPTH_CHANGE);
    ne->setMaxDepthChangeFactor(_depth_change_factor);
    // ne->setRectSize(_rect_size, _rect_size);
    ne->setBorderPolicy(ne->BORDER_POLICY_IGNORE);
    // ne->setDepthDependentSmoothing(true);
    // ne->setNormalSmoothingSize(_smoothing_size);
    ne->setInputCloud(input_cloud);
    // ne->setIndices(indices);
    ne->compute(*cloud_normals);
  };

  auto remove_shadows = [this](const pcl::PointCloud<pcl::PointXYZ>::Ptr& input_cloud,
                               pcl::PointCloud<pcl::Normal>::Ptr& input_normals,
                               pcl::PointCloud<pcl::PointXYZ>::Ptr& output_cloud) {
    // helpers::Timer tiemr("shadows: ", true);
    pcl::ShadowPoints<pcl::PointXYZ, pcl::Normal> shadow_extraction(false);
    shadow_extraction.setInputCloud(input_cloud);
    shadow_extraction.setNormals(input_normals);
    shadow_extraction.setThreshold(_shadow_treshold);
    // shadow_extraction.setNegative(true);
    shadow_extraction.filter(*output_cloud);
  };

  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
  normal_estimation(point_cloud, cloud_normals);
  remove_shadows(point_cloud, cloud_normals, out_point_cloud);

  return K4A_RESULT_SUCCEEDED;
}

k4a_result_t K4AFastPublisher::fillPointCloud(const k4a::image& pointcloud_image,
                                              pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud)
{
  // helpers::Timer timer("Filling pointcloud took", get_logger());

  const size_t point_count = pointcloud_image.get_height_pixels() * pointcloud_image.get_width_pixels();
  // pcl::PCLPointCloud2::Ptr pcl_cloud(new pcl::PCLPointCloud2);
  // pcl_cloud->data.resize(point_count*3);

  // auto iter_x = pcl_cloud->data.begin();
  // auto iter_y = pcl_cloud->data.begin() + 1;
  // auto iter_z = pcl_cloud->data.begin() + 2;

  // pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  point_cloud->points.resize(point_count);
  point_cloud->height = pointcloud_image.get_height_pixels();
  point_cloud->width = pointcloud_image.get_width_pixels();
  constexpr float kMillimeterToMeter = 1.0 / 1000.0f;
  const int16_t* point_cloud_buffer = reinterpret_cast<const int16_t*>(pointcloud_image.get_buffer());
  auto it = point_cloud->points.begin();

  for (size_t i = 0; i < point_count; i++, it++)
  {
    float z = static_cast<float>(point_cloud_buffer[3 * i + 2]);
    if (z <= 0.0f || kMillimeterToMeter * z > 2.5f)
    {
      it->x = it->y = it->z = std::numeric_limits<float>::quiet_NaN();
    }
    else
    {
      it->x = kMillimeterToMeter * static_cast<float>(point_cloud_buffer[3 * i + 0]);
      it->y = kMillimeterToMeter * static_cast<float>(point_cloud_buffer[3 * i + 1]);
      it->z = kMillimeterToMeter * z;
    }
  }

  return K4A_RESULT_SUCCEEDED;
}

k4a_result_t K4AFastPublisher::getRgbFrame(const DeviceAndParams& device,
                                           std::shared_ptr<sensor_msgs::msg::Image>& rgb_frame, bool rectified)
{
  // //helpers::Timer timer("Getting image took", get_logger());
  k4a::image k4a_bgra_frame = device.capture.get_color_image();

  if (!k4a_bgra_frame)
  {
    RCLCPP_ERROR(this->get_logger(), "Cannot render BGRA frame: no frame");
    return K4A_RESULT_FAILED;
  }

  size_t color_image_size =
      static_cast<size_t>(k4a_bgra_frame.get_width_pixels() * k4a_bgra_frame.get_height_pixels()) * sizeof(BgraPixel);

  if (k4a_bgra_frame.get_size() != color_image_size)
  {
    RCLCPP_WARN(this->get_logger(), "Invalid k4a_bgra_frame returned from K4A");
    return K4A_RESULT_FAILED;
  }

  if (rectified)
  {
    k4a::image k4a_depth_frame = device.capture.get_depth_image();

    device.calibration_data->k4a_transformation_.color_image_to_depth_camera(
        k4a_depth_frame, k4a_bgra_frame, &(device.calibration_data->transformed_rgb_image_));

    return renderBGRA32ToROS(device, rgb_frame, device.calibration_data->transformed_rgb_image_);
  }

  return renderBGRA32ToROS(device, rgb_frame, k4a_bgra_frame);
}

k4a_result_t K4AFastPublisher::renderBGRA32ToROS(const DeviceAndParams& device,
                                                 std::shared_ptr<sensor_msgs::msg::Image>& rgb_image,
                                                 k4a::image& k4a_bgra_frame)
{
  // //helpers::Timer timer("Rendering image took", get_logger());
  cv::Mat rgb_buffer_mat(k4a_bgra_frame.get_height_pixels(), k4a_bgra_frame.get_width_pixels(), CV_8UC4,
                         k4a_bgra_frame.get_buffer());

  cv::Mat rgb_converted;
  cv::cvtColor(rgb_buffer_mat, rgb_converted, CV_BGRA2BGR);

  rgb_image =
      cv_bridge::CvImage(std_msgs::msg::Header(), sensor_msgs::image_encodings::BGR8, rgb_converted).toImageMsg();

  rgb_image->header.frame_id = device.calibration_data->rgb_camera_frame_;
  rgb_image->header.stamp = timestampToROS(k4a_bgra_frame.get_device_timestamp());

  return K4A_RESULT_SUCCEEDED;
}
//------------------------------------------------------------------------------------------------------------//

// int main(int argc, char** argv)
// {
//   rclcpp::init(argc, argv);
//   K4AFastPublisher fp;
//   fp.initDevices();
//   fp.startDevices();
//   return 0;
// }

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(K4AFastPublisher)